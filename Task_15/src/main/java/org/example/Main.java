package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int[] arr = {5, 8, 11, 17, 15};
        int qeydOlunanReqem = 11;
        int index = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == qeydOlunanReqem) {
                index = i;
                break;
            }
        }

        if (index != -1) {
            System.out.println("Qeyd olunan reqemin (" + qeydOlunanReqem + ") indeksi: " + index + "-dir");
        } else {
            System.out.println("Qeyd olunan reqem: " +qeydOlunanReqem + "massivde tapilmadi");
        }
    }

}