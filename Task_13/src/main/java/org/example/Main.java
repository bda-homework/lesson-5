package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int[] arr = {5, 8, 11, 17, 15};
        int axtarilanReqem = 12;
        boolean tapildi = false;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == axtarilanReqem) {
                tapildi = true;
                break;
            }
        }

        if (tapildi) {
            System.out.println("Massivde qeyd olunan reqem var: " + axtarilanReqem);
        } else {
            System.out.println("Massivde qeyd olunan reqem tapilmadi: " +axtarilanReqem);
        }
    }

}