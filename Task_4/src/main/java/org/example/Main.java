package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        String[] users = {"Abbas", "Leman", "Xedice", "Ilyas", "Nurlan", "Nihat", "Elchin", "Murad", "Mirhesen", "Emin", "Farid", "Terane", "Murade"};

        Scanner scanner = new Scanner(System.in);
        System.out.println("Axtarish uchun ad qeyd edin: ");
        String axtardigimizAd = scanner.nextLine();

        int neticeMusbetdir = -1;
        for (int i = 0; i < users.length; i++) {
            if (users[i].toLowerCase().startsWith(axtardigimizAd.toLowerCase())) {
                System.out.println("Ad: " + users[i] + ", Indeks: " + i);
                neticeMusbetdir = 1;
            }
        }

        if (neticeMusbetdir == -1) {
            System.out.println("Bele bir user yoxdur");
        }
    }

}