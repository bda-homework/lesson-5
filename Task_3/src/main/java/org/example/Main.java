package org.example;

import java.util.Arrays;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int [] arr = {5, 7, 3, 9, 15, 26, 4};
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));
        int number = 5;
        int index = Arrays.binarySearch(arr, number);
        System.out.println(index);
    }

}