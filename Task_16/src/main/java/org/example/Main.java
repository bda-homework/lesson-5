package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int[] arr = {55, 81, 1114, 1577, 15};

        int max = arr[0];
        int min = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            } else {
                min = arr[i];
            }
        }

        System.out.println("MIN - " + min);
        System.out.println("MAX - " + max);
    }

}