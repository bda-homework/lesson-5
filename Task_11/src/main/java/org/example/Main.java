package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int [] [] arr = {
                {5, 8, 11},
                {15, 17, 15},
                {24, 78, 64}
        };

        int sumOfValues = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                sumOfValues = sumOfValues + arr[i][j];
            }
        }

        System.out.println(sumOfValues);
    }

}