package org.example;

import java.util.Arrays;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    //Task 1
    public static void main(String[] args) {
        int [] arr1 = {5, 9, 8, -1, 7, 0, 18};
        Arrays.sort(arr1);
        System.out.println(Arrays.toString(arr1));
    }

}