package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int [] [] arr = {{5, 7, 14, 20}, {15, 18, 91, 25}, {16, 74, -45, 65}};
        int sumOfElem = 0;
        for (int [] setir : arr) {
            for (int elem : setir) {
                sumOfElem = sumOfElem + elem;
            }
        }

        System.out.println("Massivin elementlerinin cemi: " + sumOfElem);
    }

}