package org.example;

import java.util.Arrays;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int [] [] arr = {
                {5, 8, 11, 17},
                {15, 17, 15, 20},
                {24, 78, 64, 91},
                {17, 38, 92, 48}
        };

        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i][arr.length - i - 1] + "");
        }

        System.out.println(Arrays.deepToString(arr));
    }

}