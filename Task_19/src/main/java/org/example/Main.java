package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        String[] strArray = {"Murad", "Fuad", "Mamed", "Fikret", "Emil", "Zaur", "Mamed"};
        System.out.println("Tekrarlanan adlar: ");

        for (int i = 0; i < strArray.length - 1; i++) {
            for (int j = i+1; j < strArray.length; j++) {
                if (strArray[i].equals(strArray[j])) {
                    System.out.println(strArray[j]);
                }
            }
        }
    }
}